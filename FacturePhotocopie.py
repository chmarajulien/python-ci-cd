""" Facture photocopie
  date : 23/10/2023
  Auteur : CHMARA Julien 
"""
while True: 
    
    n = input("Entrer le nombre de photocopie :")
    if n == '':
        break
    try:
        n = int(n)
    except ValueError:
        print("Veuillez entre une valeur numérique.")
        continue
    
    if n <= 10:
        p = n *0.1
    elif n <= 30:    
        p = 10 * 0.1 + (n - 10) * 0.09
    else:
        p = 10*0.1 + 20 * 0.09 + (n-30) * 0.08 
       
       
    print(f"Le prix total est : {round(p,2)} ") 
p += p    
    
print(f"La facture générale est de : {round(p, 2)} euro")
print("Fin de programme")