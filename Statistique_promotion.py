'''
    Programme de lecture et d'analyse d'un fichier structuré (CSV)
    statistuqes : H/F de la promotion
'''
import argparse
import sys
import datetime as dt
from dateutil import relativedelta as rd
# Import des fonctons de l'application
import fonctions.application_fonctions as af

APP_VERSION = "1.2"

parser = argparse.ArgumentParser()
parser.add_argument("-f", "--path", type=str, help="Change default file path")
parser.add_argument("-l", "--list", action='store_true',help="Display all occurences")
parser.add_argument("-v", "--version", action='store_true',help="Display version and exit")

args = parser.parse_args()
file = args.path
version = args.version
option_detail = args.list

app_name=sys.argv[0] # On trouvera toujours le nom du script/programme à l'emplacement 0 des argument de la ligne de commande

print(app_name, ":", APP_VERSION)

if version :
    sys.exit(0)

if file == None :
    file="promotion_B3_B.csv"

today=dt.datetime.now()
nbMoisTot = 0
# Boucle de lecture ligne à ligne
with open(file, 'r') as file :
    nbrf = nbrh = nbro = nbMoisTot = 0
    # On saute la premier ligne
    next(file)
    for line in file:
        if line[0] == '#':
            continue
        fields = line.strip().split(";")
        gender = fields[2]
        bdate = fields[4]
        bdate = dt.datetime.strptime(bdate,'%d/%m/%Y')
        # Partie métier
        ans, mois = af.gdelta_age(today, bdate)
        nbMoisTot+=af.nbMonths(today, bdate)
        #
        if option_detail :
            print(f"{fields[1]} - {fields[0]} - {bdate} , âge : {ans} ans et {mois} mois ({af.nbMonths(today, bdate)})")
        
        if gender.lower() == 'h':
            nbrh+=1
        elif gender.lower() == 'f':
            nbrf+=1
        else:
            nbro+=1

nbr_tot = nbrf + nbrh + nbro
sep="="*80
print(sep)
print(f"Nombre total d'élèves : {nbr_tot}")
print(f"Nombre total de filles : {nbrf} - {af.cpercent(nbrf, nbr_tot, 2)} %")
print(f"Nombre total de garçons : {nbrh} - {af.cpercent(nbrh, nbr_tot, 2)} %")
print(f"Autres : {nbro} - {af.cpercent(nbro, nbr_tot, 2)} %")

# On calule l'âge moyen de la  promotion à partir de total des mois
nbMois=int(nbMoisTot/nbr_tot)
nbAnM=nbMois//12
nbMoisM=nbMois % 12
# 
print(f"Age moyen de la promotion : {nbAnM} ans et {nbMoisM} mois ")
print(sep)
print("fin du programme...")
    
