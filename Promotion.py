"""
    Programme affichage promotion
    Auteur : CHMARA JULIEN
    Date : 16/11/2023
"""

#Lecture du fichier
with open('fichier/promotion_B3_B.csv', 'r') as fichier:
    next(fichier) #On saute la première ligne

    for ligne in fichier:
        list = ligne.split(";")
        print(f"{list[1]} / {list[0]} / {list[3]} / {list[4]}")
         #On affiche la valeur a la position 0,1 et 3